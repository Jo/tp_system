Compilation:
	make ex1
Exécution
	./a.out


Implémentation d'un minishell en C.

-- Description:

- Affichage d'un shell et gestion de l'utilisateur dans la fonction shellInput(), selon l'état de l'éxécution. Si l'ont vient d'éxécuter une commande, le booléen cmd_active sera VRAI. Alors on affichera le résultat de la commande avec son temps d'écéxution et son code retour, selon le paramètre sig_active. S'il vaut VRAI,, cela signifie que la dernière commande a été interrompue par un signal. Dans ce cas là on affiche le code signal. Sinon on affiche le code de retour envoyé par exit. 

- Le programme tourne tant que l'utilisateur n'a pas saisi la chaine: "exit".
- Un fork est appelé: le fils est chargé d'exécuter la commande de l'utilisateur et d'envoyer le retour de cette commande au père. Ce dernier récupère le status de la commande, et affiche de nouveau le shell selon le résultat de ce status. 


Remarque: 
- Le programme ne gère pas les redirections de fichiers. 
- Affichage du temps, du code retour
- Gestion des commandes complexes avec arguments

Exemple d'exécution:
hostname -i 
echo Hello World
cat text.txt
sudo su


