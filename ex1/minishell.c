
#include "minishell.h"

//--------------------------------------------------//
// Fonction qui permet d'afficher l'input 
// de l'utilisateur dans un style shell
//--------------------------------------------------//
char * shellInput(int cmd_active, int sig_active, int res, double time) {

    //--------------------------------------------------//
    // Affichage du shell selon l'état de la dernière commande 
    //--------------------------------------------------//
    if (cmd_active){
        if(sig_active){
            printf("\x1B[32m$[eistish][sign:%d|%6.2fms] \x1B[0m", res, time); //Affiche code signal + temps
        }
        else{
            printf("\x1B[32m$[eistish][exit:%d|%6.2fms] \x1B[0m", res, time); //Affiche code exit + temps 
        }
            
    }
    else{
        printf("\x1B[32m$[eistish] \x1B[0m"); //Affiche Un dollar pour simuler le shell.
    }
    

    char * input = malloc( MAX_LEN * sizeof(char));
    scanf("%[^\n]%*c", input);  //Récupérer une ligne saisie par l'utilisateur
    input[strcspn(input, "\n")] = 0; //Enlève le \n à la fin
    return input;
    }

int main(int argc, char **argv) {
    
    int pid, status, cmd_status;
    double time;
    char * input, * token;
    char separators[]   = {" "};
    
    input  = shellInput(NO_CMD, NO_SIGNAL, 0, 0); 
    token  = strtok(input, separators); //Divise la chaine input selon les espaces.

    while(strcmp(input, "exit") != 0){  //Tant que l'utilisateur n'a pas saisi la chaîne "exit"
        clock_t start_time;
        start_time  = clock();
        pid         = fork();
        
        switch(pid){
            //--------------------------------------------------//
            // Cas d'erreur du fork
            //--------------------------------------------------//
            case -1: //Cas d'erreur
                fprintf(stderr, "Erreur dans dans la fonction fork()\n");
                exit(5);
                break;

            //--------------------------------------------------//
            // Processus fils, exécute la commande de l'utilisateur
            //--------------------------------------------------//
            case 0: 
            {
                char **args = malloc(sizeof(char *));
                args[0]     = token;
                int nbArgs  = 1;
                char *arg;
                //--------------------------------------------------//
                // Gestion des arguments de la commande
                //--------------------------------------------------//
                do{
                    arg     = strtok(NULL, " ");
                    args    = realloc(args, sizeof(char*) * ++nbArgs);
                    args[nbArgs-1] = arg;
                } while(arg);
                
                execvp(args[0], args);
                printf("Recouvrement du code %s impossible: \n", args[0]);
                exit(1);
                break;
            }
            //--------------------------------------------------//
            // Processus père, récupère l'état de retour de la commande
            //--------------------------------------------------//
            default:
            {
                wait(&status);
                time        = 1000 * (double)(clock()-start_time)/CLOCKS_PER_SEC;

                //--------------------------------------------------//
                //Si la commande s'est terminée normalement (code exit)
                //--------------------------------------------------//
                if (WIFEXITED(status)) {
                    cmd_status = WEXITSTATUS(status);  //code retour de la commande
                    input      = shellInput(RETOUR_CMD, NO_SIGNAL, cmd_status, time);
                }
                else {
                    //--------------------------------------------------//
                    //Si la commande s'est terminée par un signal
                    //--------------------------------------------------//
                    if (WIFSIGNALED(status)){ 
                    cmd_status = WTERMSIG(status); //on renvoie le code du signal qui a terminé la commande
                    input      = shellInput(RETOUR_CMD, SIGNAL, cmd_status, time);
                    }
                }
                token       = strtok(input, " ");
            }
        }
        
    }
    return 0;
}
