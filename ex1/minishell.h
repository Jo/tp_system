
/*! \file triFusion_par.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Jeudi 9 Avril 2019
 *  \brief Mini shell
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>

#define MAX_LEN 80
#define NO_SIGNAL 0
#define SIGNAL 1
#define RETOUR_CMD 1
#define NO_CMD 0

/*! 
 * \fn char * shellInput(int cmd_active, int res, double time) 
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Dimanche 5 Avril 2019
 *
 * \brief Fusionne deux sous-tableaux déjà triés le tableau res.
 * 
 * \param cmd_active: booléen qui détermine si on affiche un retour de commande ou non
 * \param sig_active: booléen qui détermine si on affiche un retour de signal
 * \param res       : code retour de commande à affciher si cmd_active est vrai
 * \param time      : temps d'éxécution de la commande précédente
 * \remarks
 * Dans le cas où il n'y a pas de commande précédente, mettre les 4 arguments à 0.
 * Si cmd_active est VRAI:
 *      Si sig_active est VRAI:
 *          retourner code signal
 *      Sinon
 *          retourner code exit.
 */
char * shellInput(int cmd_active, int sig_active, int res, double time);