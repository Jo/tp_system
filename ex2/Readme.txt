Compilation:
    make ex2
Exécution
    ./a.out


Ce programme est une solution du problème du dîner des philosophes. Il s'agit de synchroniser le moment ou chaque philosophe mange. 


-- Fonctionnement:
Le programme utilise N+1 sémaphores: 

- un sémaphore par fourchette (il y a N fourchettes) pour contrôler l'accès à chaque fourchette (mutex)

- un sémaphore pour contrôler qu'au maximum N-1 philosophes rentrent dans la zone protégée. Car sinon, il peut y avvoir un blocage, lorsque les N threads demandent l'accès à la fourchette gauche, et alors aucun philosophe ne pourrait manger.



-- Affichage éxécution:

Le philosophe 1 mange avec les fourchettes 1 et 2 
Le philosophe 3 mange avec les fourchettes 3 et 4 
Le philosophe 1 a fini de manger
Le philosophe 3 a fini de manger
Le philosophe 0 mange avec les fourchettes 0 et 1 
Le philosophe 2 mange avec les fourchettes 2 et 3 
Le philosophe 2 a fini de manger
Le philosophe 0 a fini de manger
Le philosophe 4 mange avec les fourchettes 4 et 0 
Le philosophe 1 mange avec les fourchettes 1 et 2 
Le philosophe 1 a fini de manger
Le philosophe 4 a fini de manger
Le philosophe 3 mange avec les fourchettes 3 et 4 
Le philosophe 0 mange avec les fourchettes 0 et 1 
Le philosophe 0 a fini de manger
Le philosophe 3 a fini de manger
Le philosophe 2 mange avec les fourchettes 2 et 3 
Le philosophe 4 mange avec les fourchettes 4 et 0


-- Remarques: 
- TOUS les philosophes mangent (de 0 à N-1: 5 philosophes)
- Il faut peut être laisser le programme s'éxécuter quelques secondes pour voir apparaître tous les philosophes
- Seuls les philosophes dont la fourchette droite ET la fourchette gauche sont disponibles, peuvent manger.
- Le philosophe i mange avec la fourchette i et la fourchette (i+1) modulo N ==> Crée une boucle (=table ronde)
