
 #include "diner_philo.h"


//-------------------------------------------------------------//
// Fonction qui contient la zone protégée, et qui synchronise
// les threads grâce à deux sémaphores.
//-------------------------------------------------------------//
void* do_something (void * num)
{
    int _num = *(int *)num;
    while(1){
        sem_wait(&sem);        
        sem_wait(&fourchettes[_num]);       
        sem_wait(&fourchettes[(_num+1)%N]);
        sem_post(&sem);
        printf("Le philosophe %d mange avec les fourchettes %d et %d \n", _num, _num, (1+_num)%N );
        sleep(1 + (int)(N*rand()/(RAND_MAX+1.0)));
        printf("Le philosophe %d a fini de manger\n", _num);
        sem_post(&fourchettes[_num]);
        sem_post(&fourchettes[(_num+1)%N]);

        
    }
    return NULL;
}

int main (void)
{
    int j;
    int num_p[N];

    //-------------------------------------------------------------//
    // Définition d'une sémaphore par fourchette
    // La fourchette est tantôt accessible, tantôt inaccessible
    //-------------------------------------------------------------//
    for(int i =0; i<N; i++){
        sem_init(&fourchettes[i], 1, 1);
    }
    //-------------------------------------------------------------//
    // Sémaphore qui contrôle qu'il y ait au maximum 4 philosophes 
    // dans la zone protégée pour éviter l'interblocage
    //-------------------------------------------------------------//
    sem_init(&sem, 1 , N-1);        
    pthread_t threads[5];

    for (int i=0; i<N; i++){
        j = i;
        num_p[j] = j;
        //-------------------------------------------------------------//
        // Création des threads qui représentent les philosophes
        //-------------------------------------------------------------//
        if (pthread_create(&threads[j], NULL, do_something, &num_p[j] )) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
        
    }
    //-------------------------------------------------------------//
    // Fonction qui suspend l'éxécution du thread appelant jusqu'à
    // que le thread thread[j] ait terminé.
    //-------------------------------------------------------------//
    for (int i = 0; i<N; i++){
        
        if (pthread_join(threads[j], NULL)){
            perror("pthread_join");
        }

    }
  
  printf("End execution\n") ;
    return (EXIT_SUCCESS);

    
}

