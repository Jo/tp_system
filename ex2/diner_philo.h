
/*! \file diner_philo.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Jeudi 9 Avril 2019
 *  \brief Solution du problème du dîner des philosophes
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#define N 5

sem_t sem; // Sémaphore qui controle qu'au maximum N-1 threads demandent la fourchette pour éviter l'interblocage
sem_t fourchettes[N]; //Sémaphore qui permet de contrôler la disponibilité des fourchettes 

/*! 
 * \fn void* do_something (void * num);
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Jeudi 9 Avril 2019
 *
 * \brief Fusionne deux sous-tableaux déjà triés le tableau res.
 * 
 * \param void * num: paramètre qui permet d'identifier le philosophe en cours
 * \remarks
 */
void* do_something (void * num);