Ce programme est l'implémentation avec des sockets, d'un miniserveur HTTP qui affiche une image ou une vidéo au client

Compilation:
    make ex3
Exécution:
    ./a.out

Fonctionnement:
    Dans un navigateur, taper l'adresse: 
    localhost:8080

    Il est possible d'accéder directement aux ressources sur le serveur:
    localhost:8080/favicon.ico
    localhost:8080/doctest.jpg
    localhost:8080/video2.jpg

Les étapes de création du serveur sont: 

- Création de la socket serveur et de ses paramètres

- Définition de l'adresse serveur de la socket, et de ses paramètres (port 8080)

- Association de l'adresse à la socket

- Ecoute de la socket, avec au maximum 10 clients

- Lancement d'une boucle infinie pour pouvoir se connecter et de déconnecter indéfiniment au serveur.

- Acceptation d'une connexion cliente

- Parsing traitement de la requête HTTP cliente de manière récursive et envoi de la réponse:
La réponse enclenche de nouvelles requêtes pour avoir accès aux ressources telles que les images ou les vidéos

- Affichage de la page chez le client



