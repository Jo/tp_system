#include "webserver.h"


//-------------------------------------------------------------//
// Fonction qui retourne la taille d'un fichier
//-------------------------------------------------------------//
unsigned long fsize(char * file)
{
    FILE * f = fopen(file, "r");
    fseek(f, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    fclose(f);
    return len;
}

int main(int argc, char *argv[])
{
    struct sockaddr_in server_addr, client_addr;
    socklen_t sin_len = sizeof(client_addr);
    int fd_server , fd_client;
    char buf[2048];
    int fd_img;

    //-------------------------------------------------------------//
    // Création socket
    //-------------------------------------------------------------//
    fd_server = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_server < 0){
        perror("socket\n");
        exit(1);
    }

    
    //-------------------------------------------------------------//
    //Initialisation de l'adresse serveur de la socket
    //-------------------------------------------------------------//
    setsockopt(fd_server, SOL_SOCKET, SO_REUSEADDR, 1, sizeof(int));
    server_addr.sin_family      = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port        = htons(8080);


    
    //-------------------------------------------------------------//
    //Association entre l'adresse et la socket
    //-------------------------------------------------------------//
    if(bind(fd_server, (struct sockaddr *) &server_addr, sizeof(server_addr)) == 1)
    {
        perror("bind");
        close(fd_server);
        exit(1);
    }

    //-------------------------------------------------------------//
    //Le serveur ecoute et peut avoir 10 clients maximum
    //-------------------------------------------------------------//
    if (listen(fd_server, 10) == -1)
    {
        perror("listen");
        close(fd_server);
        exit(1);
    }

    //-------------------------------------------------------------//
    // Boucle infinie, serveur toujouts à l'écoute de nouvelles 
    // connexions
    //-------------------------------------------------------------//
    while(1)
    {
        //Le serveur accepte un client. La fonction retourn un descripteur de fichier
        fd_client = accept(fd_server, (struct sockaddr *) &client_addr, &sin_len);
        
        if (fd_client == 1)
        {
            perror("La connexion a échoué.. \n");
            continue;
        }

        printf("Client connecté\n");

        //-------------------------------------------------------------//
        // Traitement des requêtes GET
        //-------------------------------------------------------------//
        if (!fork()) //équivalent à if (fork == 0) ==> processus fils
        {
            close(fd_server);
            memset(buf, 0, 4096);
            read(fd_client, buf, 4095);
            printf("%s\n", buf);


            //-------------------------------------------------------------//
            // Traitement selon la ressource demandée
            //-------------------------------------------------------------//



            //-------------------------------------------------------------//
            // Images
            //-------------------------------------------------------------//
            if(!strncmp(buf, "GET /favicon.ico", 16))
            {
                printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                write(fd_client, imageheader, sizeof(imageheader) - 1);
                fd_img = open("favicon.ico", O_RDONLY);
                int sent = sendfile(fd_client, fd_img, NULL, fsize("doctest.jpg")); 
                printf("sent: %d\n", sent);
                close(fd_img);
            }
            else if (!strncmp(buf, "GET /doctest.jpg", 16))
            {
                printf("!!!!!!!!!!!!!!!!!!!! IMAGE !!!!!!!!!!!!!!!!!!!!\n");
                write(fd_client, imageheader, sizeof(imageheader) - 1);
                fd_img = open("doctest.jpg", O_RDONLY);
                int sent = sendfile(fd_client, fd_img, NULL, fsize("doctest.jpg")); 
                printf("sent: %d\n", sent);
                close(fd_img);
            }


            //-------------------------------------------------------------//
            // Vidéo
            //-------------------------------------------------------------//
            else if (!strncmp(buf, "GET /video2.mp4", 15))
            {
                printf("!!!!!!!!!!!!!!!!! VIDEO !!!!!!!!!!!!!!!!!!!!!!!\n");
                write(fd_client, videoheader, sizeof(videoheader) - 1);
                fd_img = open("video2.mp4", O_RDONLY);
                int sent = sendfile(fd_client, fd_img, NULL, fsize("video2.mp4")); 
                printf("sent: %d\n", sent);
                close(fd_img);
            }

            //-------------------------------------------------------------//
            // Envoi de la réponse HTML
            // Celle-ci demande les ressources ci-dessus
            //-------------------------------------------------------------//
            else{
                write(fd_client, response, sizeof(response)-1);
            }
            close(fd_client);
            printf("En cours de fermeture..\n");
            exit(0);
        }
        close(fd_client);
    }

    return 0;
}