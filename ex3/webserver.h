/*! \file webserver.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Jeudi 9 Avril 2019
 *  \brief Implémentation d'un mini serveur web qui affiche une image et une vidéo au client
 */



#include <sys/socket.h>
#include <sys/sendfile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h> 




/*! 
 * \fn unsigned long fsize(char * file);
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Jeudi 9 Avril 2019
 *
 * \brief Fonction qui retourne la taille d'un fichier
 * 
 * \param file: chemin du fichier dont on veut connaître la taille.
 * \remarks
 */
unsigned long fsize(char * file);


//-------------------------------------------------------------//
// Réponse envoyée au client au format HTML
//-------------------------------------------------------------//
char response[] = 
"HTTP/1.1 200 Ok\r\n"
"Content-Type: text/html; charset=UTF-8\r\n\r\n"
"<!DOCTYPE html>\r\n"
"<html><head><title>Serveur Web en C</title>\r\n"
"<style>body { background-color: #A9D0F5 }</style></head>\r\n"
"<body><center> <h1>Hello World!</h1><br>\r\n"
" <img src=\"doctest.jpg\"/> <br>"
"<video width=\"320\" height=\"240\" controls>"
  "<source src=\"video2.mp4\" type=\"video/mp4\">"
"Your browser does not support the video tag. </video> </center></body></html>\r\n";


//-------------------------------------------------------------//
// Envoi de l'image au client
//-------------------------------------------------------------//
char imageheader[] = 
"HTTP/1.1 200 Ok\r\n"
"Content-Type: image/jpeg\r\n\r\n";


//-------------------------------------------------------------//
// Envoi d'une vidéo au client
//-------------------------------------------------------------//
char videoheader[] = 
"HTTP/1.1 200 Ok\r\n"
"Content-Type: video/mpeg\r\n\r\n";



